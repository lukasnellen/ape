"""
Build support for the aires_ air shower simulation package
-------------------------------------------------------------
"""
import os.path
import sys
import re

from ApeTools import Config, Build
from ApeTools.Build import call


class Aires(Build.Package):
    def __init__(self, name, builder):
        """Set up additional attributes:

        :attr:`fc`
          The fortran compiler
        :attr:`prefixBase`
          The base of the prefix, without version number. Needed for
          configuration.
        """
        Build.Package.__init__(self, name, builder)
        self.setAttributes("fc")
        # remove the version number
        self.prefixBase = os.path.dirname(self.prefix)
        self.configRePattern = r'^#?(%s)\s*=(.*)'

    def configure(self, _logName, _env):
        """Generate the ``config`` file in the aires_ top level
        directory.

        The package variable ``fc`` can be used to select the Fortran
        compiler used to build aires_.
        """
        configParams = {
            "Platform":  'Linux',
            "AiresRootDir":  self.prefixBase,
            "FortCompile":  '"%s"' % self.fc
            }
        if sys.platform == "darwin":
            configParams["ArchiveCreate"] = '"libtool -static -o"'

        print "  ... Configuring"
        configOutName = os.path.join(self.buildDirectory, "config")
        if Config.getboolean("ape", "verbose"):
            print "      Creating", configOutName
        if Config.getboolean("ape", "dryRun"):
            return
        configIn = open(os.path.join(self.buildDirectory, "config.original"))
        configOut = open(configOutName, "w")
        pattern = re.compile(self.configRePattern %
                             '|'.join(configParams.keys()))
        for l in configIn:
            matches = pattern.match(l)
            if matches is not None:
                print >> configOut, "%s=%s" % (matches.group(1),
                                               configParams[matches.group(1)])
            else:
                print >> configOut, l[:-1]
        configOut.close()
        configIn.close()

    def make(self, logName, env):
        """Run the :command:`doinstall` script to build and install Aires_.
        """
        try:
            del env["AIRESHOME"]
        except KeyError:
            pass

        # Builds and installs in one step.
        print "  ... Building and installing"
        buildCmd = ["./doinstall", "0"]
        try:
            call(buildCmd, cwd=self.buildDirectory, output=logName,
                 package="aires", stage="build+install", env=env)
        except:
            self.removePrefixDir()
            raise
