#! /usr/bin/env python

# Configure path to find modules to test
import sys
import os.path
if __name__ == "__main__":
    sys.path.insert(0, os.path.dirname(sys.path[0]))

import unittest
from ApeTools import InstallError


def raiseError(args=[], cwd=None, pack=None, stage=None, log=None, cmd=None):
    raise InstallError(args, cwd, pack, stage, log, cmd)


class testError(unittest.TestCase):
    def testRaise(self):
        self.assertRaises(InstallError, raiseError)

    def testPackage(self):
        try:
            raiseError(pack="TestPackage")
        except InstallError, e:
            msg = str(e)
        self.assertEqual(msg, "  *** Package: TestPackage\n")

    def testArgs(self):
        try:
            raiseError(args=['a1', 'a2'])
        except InstallError, e:
            msg = str(e)
        self.assertEqual(msg, "  *** Extra arguments:\n    a1, a2")

    def testArgsPackage(self):
        try:
            raiseError(args=['a1', 'a2'], pack="TestPackage")
        except InstallError, e:
            msg = str(e)
        self.assertEqual(msg, "  *** Package: TestPackage\n" +
                         "  *** Extra arguments:\n    a1, a2")

if __name__ == '__main__':
    unittest.main()
